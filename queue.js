let collection = [];

// Write the queue functions below.

//*  Print queue elements
function print() {
  return collection;
}

//*  Enqueue
function enqueue(name) {
  collection.push(name);
  return collection;
}

//* Dequeue
function dequeue() {
  collection.shift();
  return collection;
}

//* Get first element
function front() {
  return collection[0];
}

//* Get queue size
function size() {
  return collection.length;
}

//* Check if queue is not empty
function isEmpty() {
  if (collection.length === 0) {
    return true;
  } else {
    return false;
  }
}

module.exports = {
  collection,
  print,
  enqueue,
  dequeue,
  front,
  size,
  isEmpty,
};
